# Image + Prompt to Image service

### Installation Guide
Python version: 3.11
* Install [Pytorch](https://pytorch.org/)
* Install [HuggingFace Hub](https://huggingface.co/docs/transformers/installation)
* Install [Flask](https://flask.palletsprojects.com/en/3.0.x/installation/)
* pip install transformers deep-translator numpy pillow