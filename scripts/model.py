from PIL import Image
from deep_translator import GoogleTranslator
import torch
from transformers import AutoProcessor, LlavaForConditionalGeneration


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


class ImageToTextModel:

    def __init__(
            self
    ):
        self.num_tokens = 200

        self.model_id = "llava-hf/llava-1.5-7b-hf"
        self.model = LlavaForConditionalGeneration.from_pretrained(
            self.model_id,
            torch_dtype=torch.float16,
            low_cpu_mem_usage=True,
            load_in_4bit=True
        )
        self.processor = AutoProcessor.from_pretrained(self.model_id)

    def predict(
            self, image,
            prompt: str,
            language_in: str = "en",
            language_out: str = "cs"
    ) -> str:
        if language_in != "en":
            prompt = GoogleTranslator(source=language_in, target="en").translate(prompt)

        inputs = self.processor(prompt, image, return_tensors='pt').to(0, torch.float16)
        output = self.model.generate(**inputs, max_new_tokens=self.num_tokens, do_sample=False)
        prediction = self.processor.decode(output[0][2:], skip_special_tokens=True)

        if language_out != "en":
            prediction = GoogleTranslator(source="en", target=language_out).translate(prediction)

        return prediction


def test_model():
    import requests

    prompt = "USER: <image>\nWhat are these?\nASSISTANT:"

    model = ImageToTextModel()

    image_file = "http://images.cocodataset.org/val2017/000000039769.jpg"
    image = Image.open(requests.get(image_file, stream=True).raw)
    print(model.predict(image, prompt))
    image = Image.open(open("../data/IMG_20240204_191046.jpg", "rb"))
    prediction = model.predict(image, "USER: <image>\nDescribe objects in the image and their locations.\nASSISTANT:")
    print(prediction)
