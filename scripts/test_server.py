import time
import json
import requests
import numpy as np
from PIL import Image


def send_image_to_server(image_path, server_url):
    for i in range(10):
        try:
            start_time = time.time()
            with open(image_path, "rb") as image_file:

                response = requests.post(
                    server_url,
                    files={"image": image_file},
                    data={
                        "prompt":
                            "USER: <image>\nDescribe foreground and background objects in the image and their locations. "
                            "Output format: 'Foreground: <description>', 'Background: <description>'"
                            "\nASSISTANT:",
                        "language_in": "en",
                        "language_out": "cs",
                    },
                )
                print(response.text)
            print("Elapsed time: ", time.time() - start_time)

        except Exception as e:
            print(f"An error occurred: {e}")


if __name__ == "__main__":
    server_url = 'http://localhost:5001/process_image'

    image_path = '../data/IMG_20240204_191046.jpg'
    send_image_to_server(image_path, server_url)
