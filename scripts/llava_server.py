from PIL import Image
from flask import Flask, request, jsonify
from model import ImageToTextModel

app = Flask(__name__)

model = ImageToTextModel()

@app.route("/")
def hello():
    return "Wellcome to the LLaVa server!"


@app.route("/process_image", methods=["POST"])
def process_image():
    if request.method == "POST":
        try:
            image_path = request.files["image"].filename
            with open(image_path, "wb") as image_file:
                image_file.write(request.files["image"].read())
        except Exception as e:
            print(f"An error occurred: {e}")

        image_stream = request.files["image"]
        image = Image.open(image_stream.stream)
        prompt = request.form.get("prompt")
        language_in = request.form.get("language_in")
        language_out = request.form.get("language_out")
        prediction = model.predict(image, prompt, language_in, language_out)
        return prediction
    else:
        return "Only POST requests are accepted"


if __name__ == "__main__":
    app.run(debug=False, port=5001, host="localhost")

