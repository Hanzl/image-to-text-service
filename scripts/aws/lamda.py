import sys

import boto3
import json
import time
import base64


try:
    bedrock = boto3.client(
        service_name='bedrock-runtime',
        aws_access_key_id="",
        aws_secret_access_key="",
        region_name="us-west-2"
    )

    model_settings = {
        "modelId": "anthropic.claude-3-sonnet-20240229-v1:0",
        "accept": 'application/json',
        "contentType": 'application/json'
    }
except Exception as e:
    print(f"An error occurred: {e} during client setup")
    sys.exit(1)


def create_bedrock_request(
        image: str,
        prompt: str,
        language_in: str,
        language_out: str,
        max_len: int = 256,
) -> str:

    bedrock_request = {
        "anthropic_version": "bedrock-2023-05-31",
        "max_tokens": max_len,
        "messages": [
            {
                "role": "user",
                "content": [
                    {
                        "type": "image",
                        "source": {
                            "type": "base64",
                            "media_type": "image/jpeg",
                            "data": image
                        }
                    },
                    {
                        "type": "text",
                        "text": prompt
                    }
                ]
            }
        ]
    }

    return json.dumps(bedrock_request)


def timer(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        output = func(*args, **kwargs)
        print(time.time() - start_time)
        return output
    return wrapper


def load_image_base64(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode("utf-8")


@timer
def process_image(
        image: str,
        prompt: str,
        language_in: str = "en",
        language_out: str = "cs",
        max_len: int = 256,
):
    body = create_bedrock_request(
        image,
        prompt,
        language_in,
        language_out,
        max_len,
    )
    response = bedrock.invoke_model(body=body, **model_settings)

    response_body = json.loads(response.get('body').read())

    message_response = response_body.get("content")[0].get("text")

    return message_response


if __name__ == "__main__":

    image = load_image_base64("../../data/422750625_941855690787873_3009531611644974797_n.jpg")
    prompt = "USER: <image>\nDescribe foreground and background objects in the image and their locations. " \
             "Output format: 'Foreground: <description>', 'Background: <description>'\nASSISTANT:"

    for i in range(10):
        response_message = process_image(image, prompt, max_len=256)
        print(response_message)


