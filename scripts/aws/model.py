import sys

import boto3
import json

from utils import load_image_base64, timer

# from deep_translator import GoogleTranslator

TRANSLATE = True

try:
    bedrock = boto3.client(
        service_name='bedrock-runtime',
        region_name="us-west-2"
    )

    model_settings = {
        "modelId": "anthropic.claude-3-sonnet-20240229-v1:0",
        "accept": 'application/json',
        "contentType": 'application/json'
    }

    translator = boto3.client('translate')
except Exception as e:
    print(f"An error occurred: {e} during client setup")
    sys.exit(1)


def create_bedrock_request(
        image: str,
        messages: list,
        language_in: str,
        language_out: str,
        max_len: int = 256,
) -> str:
    bedrock_request = {
        "anthropic_version": "bedrock-2023-05-31",
        "max_tokens": max_len,
        "messages": messages
        # [
        #     {
        #         "role": "user",
        #         "content": [
        #             {
        #                 "type": "image",
        #                 "source": {
        #                     "type": "base64",
        #                     "media_type": "image/png",
        #                     "data": image,
        #                 },
        #             },
        #             {"type": "text", "text": prompt},
        #         ],
        #     }
        # ],
    }

    return json.dumps(bedrock_request)


def translate(
        text: str,
        language_in: str,
        language_out: str,
) -> str:
    return GoogleTranslator(source=language_in, target=language_out).translate(text)


def aws_translate(
        text: str,
        language_in: str,
        language_out: str,
) -> str:
    response = translator.translate_text(
        Text=text,
        SourceLanguageCode=language_in,
        TargetLanguageCode=language_out
    )

    translated_text = response['TranslatedText']
    print(translated_text)
    return translated_text


def aws_translate_messages(
        messages: dict,
        language_in: str,
        language_out: str,
) -> dict:
    for message in messages:
        content = message.get("content")
        for con in content:
            if con.get('type') == 'text':
                text = con.get('text')
                if language_in != 'en' and text:
                    con['text'] = aws_translate(text, language_in, language_out)

    return messages


@timer
def process_image(
        image: str,
        messages: list,
        language_in: str = "en",
        language_out: str = "cs",
        max_len: int = 256,
):
    try:
        if language_in != 'en' and TRANSLATE:
            messages = aws_translate_messages(messages, language_in, "en")

        body = create_bedrock_request(
            image,
            messages,
            language_in,
            language_out,
            max_len
        )

        response = bedrock.invoke_model(body=body, **model_settings)

        response_body = json.loads(response.get('body').read())

        message_response = response_body.get("content")[0].get("text")

        if language_out != 'en' and TRANSLATE:
            message_response = aws_translate(message_response, "en", language_out)

    except Exception as e:
        return str(e)

    return message_response
