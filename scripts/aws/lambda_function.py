import json
from model import process_image

PASSWORD = 'bajsdpf4545'


def lambda_handler(event, context):
    request_data = json.loads(event.get("body"))
    print(request_data)

    messages = request_data.get("messages")
    image = request_data.get("image")
    prompt = request_data.get("prompt")
    max_length = request_data.get("max_length")
    language_in = request_data.get("language_in")
    language_out = request_data.get("language_out")
    password = request_data.get('password')

    if password != PASSWORD:
        return {
            "statusCode": 403,
            "headers": {
                "Content-Type": "application/json",
                "charset": "utf-8"
            }
        }

    response_message = process_image(
        image,
        messages,
        language_in,
        language_out,
        max_length
    )

    print("Response", response_message)

    response_body = {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json",
            "charset": "utf-8"
        },
        "body": json.dumps({
            "response_message": response_message
        }, ensure_ascii=True)
    }

    print("Response Body: ", response_body)

    return response_body
